import React from 'react'

function About() {
    return (
        <div style={{marginTop:"40px", padding:"50px", border:"1px solid #ccc"}}>
            <h1 style={{textAlign:"center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <ol>
                <li><strong style={{width:"100px"}}>Nama:</strong> Giran</li> 
                <li><strong style={{width:"100px"}}>Email:</strong> klikace123@gmail.com</li> 
                <li><strong style={{width:"100px"}}>Sistem Operasi yang digunakan:</strong> windows 10</li>
                <li><strong style={{width:"100px"}}>Akun Gitlab:</strong> https://gitlab.com/giran_user</li> 
                <li><strong style={{width:"100px"}}>Akun Telegram:</strong> @giran_bdg</li> 
            </ol>
        </div>
    )
}

export default About
