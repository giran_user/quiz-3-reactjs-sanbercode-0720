import React, {Component} from 'react';
import Axios from "axios";
import { Redirect } from 'react-router-dom'
import { urlApi } from "./urlApi";
 
class Login  extends Component {
    state = {
        isComplete: null
      };
    
      onLoginBtnClick = () => {
        let inputEmail = this.refs.email.value;
        let inputPassword = this.refs.password.value;
    
        if (inputEmail && inputPassword) {
          Axios.get(`${urlApi}users?email=${inputEmail}&password=${inputPassword}`)
            .then(res => {
              if (res.data.length > 0) {
              console.log(res)
                
              var dataUser = res.data[0]
              this.props.bebas(dataUser)
              localStorage.setItem('id',res.data[0].id)
              console.log(res.data)
              if (res.data[0].role) {
                this.setState({isComplete : true})
                
                
              }else{
                this.setState({isComplete :false})
                
    
              }              
    
              } else {
                return("Error");
              }
            })
            .catch(err => {
              console.log(err);
            });
        } else {
          return("Error");
        }
      };
    
    render() { 
        return (
            <div className="container">
                <div className="row justify-content-center">
                        <div className="col-md-6">
                        <h2 style={{textAlign:"center"}}>Login Sistem</h2>
                            <form>
                                <div className="form-group">
                                    <label for="exampleInputEmail1">Username </label>
                                     <input type="email" className="form-control" placeholder="Masukan username anda"  required/>
                                </div>
                                <div className="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" className="form-control" placeholder="Password" />
                                </div>
                                <div class="form-check">
                                    <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
                                    <label className="form-check-label" for="exampleCheck1">Check me out</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>   
                            </form>
                        </div>
                </div>
            </div> 
         );
    }
}
 
export default Login ;