import React from "react"
import { Link } from "react-router-dom";
import "./Nav.css"
import logo from "./img/logo.png"



const Nav = () =>{
  

  return(
    <nav style={{background:"white"}}>
      <img src={logo} alt="Logo" style={{width:"200px"}} />
      <ul className="kanan">
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/movieListEditor">Movie List Editor</Link>
        </li>
        <li>
          <Link to="/login">Login</Link>
        </li>        
      </ul>
    </nav>
  )
}

export default Nav