import React, { Component } from 'react';
import axios from 'axios';
import Background from "./img/pattern.jpg"

var sectionStyle = {
    backgroundImage: `url(${Background})`
};

class Home extends Component {
    state = {
        movies : []
    }

    componentDidMount() {
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
            const movies = res.data;
            this.setState({ movies });
        })
    }

    



    // <ul>
    //     { this.state.movies.map(movie => <li>{movie.title}</li>)}
    // </ul>

    render() {
        return (
            <>
                <section style={ sectionStyle }>
                    <div style={{marginLeft:"200px", marginRight:"200px", marginTop:"50px", backgroundColor:"white"}}>                        
                        <h1 style={{textAlign:"center"}} >Daftar Film Film Terbaik</h1>
                        
                            {
                                this.state.movies.map((movie, index)=>{
                                    return(                    
                                        <div>                                            
                                            <p style={{paddingBottom:"20px", color:"purple", fontSize:"20px"}} >{movie.title}</p>                                        
                                            <p style={{fontWeight:"bold"}} >Rating : {movie.rating}</p>
                                            <p style={{fontWeight:"bold"}} >Durasi : {movie.duration}</p>
                                            <p style={{paddingBottom:"15px", fontWeight:"bold"}} >Genre : {movie.genre}</p>  
                                            <p><span style={{fontWeight:"bold"}}>Deskripsi :</span> {movie.description}</p>
                                            <hr/> 
                                        </div>
                                        
                                    )
                                })
                                

                                
                            }    
                    </div>
                    <footer style={{border:"1px solid black", backgroundColor:"black"}}>
                        <h5 style={{color:"white", textAlign:"center"}} >copyright &copy; 2020 by Sanbercode</h5>
                    </footer>
                </section>
                
            </>
        );
    }
}

export default Home;