import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from './Home';
import About from './About';
import Movie from './MovieListEditor';
import Login from './Login';
import Nav from './Nav';


export default function App() {
  return (
      <>
                  
          <Nav/>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>

            <Route path="/about">
              <About />
            </Route>
            
            <Route path="/movieListEditor">
              <Movie />
            </Route>

            <Route path="/login">
              <Login />
            </Route>           
          
          </Switch>
        
      </>
  );
}